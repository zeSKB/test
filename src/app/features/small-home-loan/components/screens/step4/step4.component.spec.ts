import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step4ScreenComponent } from './step4-screen.component';

describe('Step4ScreenComponent', () => {
  let component: Step4ScreenComponent;
  let fixture: ComponentFixture<Step4ScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step4ScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step4ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
