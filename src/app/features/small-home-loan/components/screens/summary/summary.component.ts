import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { 
  getStep1Data, 
  getStep2Data, 
  getStep3Data, 
  getStep4Data, 
  getStep5Data, 
  getStep6Data 
} from '../../../selectors';
import { take } from 'rxjs/operators';

interface QuestionAnswer {
  question: string;
  answer: string;
}


const transformData = (data: Record<string, string>): QuestionAnswer[] =>
  Object.keys(data).map((key) => ({
    question: FormFieldToLabelMap[key],
    answer: data[key]
  }));

@Component({
  selector: 'summary-screen',
  templateUrl: './summary.component.html',
  styleUrls: ['./summary.component.css']
})
export class SummaryComponent implements OnInit {
  fields: Array<QuestionAnswer> = [];

  constructor(private _router: Router, private _store: Store<AppState>) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);

    // look away
    [getStep1Data, getStep2Data, getStep3Data, getStep4Data, getStep5Data, getStep6Data].forEach((selector: any) => {
      this._store
        .select(selector)
        .pipe(take(1))
        .subscribe((data: object) => {
          this.fields = [
            ...this.fields,
            ...transformData(data as Record<string, string> || {})
          ]
        });
    })
  }

  onClickedNext() {
    this._router.navigate([StepRoutes.End]);
  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Step6]);
  };

}
