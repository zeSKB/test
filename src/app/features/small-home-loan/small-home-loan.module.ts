import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IntroductionComponent } from './components/screens/introduction/introduction.component';
import { SummaryComponent } from './components/screens/summary/summary.component';
import { Step1Component } from './components/screens/step1/step1.component';
import { Step2Component } from './components/screens/step2/step2.component';
import { Step3Component } from './components/screens/step3/step3.component';
import { Step4Component } from './components/screens/step4/step4.component';
import { FormStepComponent } from './components/form-step/form-step.component';
import { Step5Component } from './components/screens/step5/step5.component';
import { Step6Component } from './components/screens/step6/step6.component';

const formScreenComponents = [
  IntroductionComponent,
  SummaryComponent,
  Step1Component,
  Step2Component,
  Step3Component,
  Step4Component,
  Step5Component,
  Step6Component,
  FormStepComponent
];

@NgModule({
  declarations: formScreenComponents,
  exports: formScreenComponents,
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
  ]
})
export class SmallHomeLoanModule { }
