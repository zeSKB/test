import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep1Data } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { LoanInfo } from '../../../types';
import { FormFieldToLabelMap } from '../../../constants';
import { updateLoanInfoData } from '../../../actions';

@Component({
  selector: 'step1-screen',
  templateUrl: './step1.component.html',
  styleUrls: ['./step1.component.css']
})
export class Step1Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      amount: [null, [Validators.required]]
    },
    {
      updateOn: 'blur'
    }
  );

  loanAmountControl = this.stepForm.get('amount');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);

    this._store
      .select(getStep1Data)
      .pipe(take(1))
      .subscribe(
        (data: LoanInfo) => this.stepForm.patchValue(data || {}, { emitEvent: false })
      );

    this.loanAmountControl?.valueChanges.subscribe(
      (value: string) => this._store.dispatch(updateLoanInfoData({ payload: {amount: value}}))
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    this._router.navigate([StepRoutes.Step2]);
  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Introduction]);
  };

}
