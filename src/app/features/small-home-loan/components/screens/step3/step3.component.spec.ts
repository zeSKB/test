import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { Step3ScreenComponent } from './step3-screen.component';

describe('Step3ScreenComponent', () => {
  let component: Step3ScreenComponent;
  let fixture: ComponentFixture<Step3ScreenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ Step3ScreenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(Step3ScreenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
