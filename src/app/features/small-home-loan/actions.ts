import { createAction, props, ActionCreator } from '@ngrx/store';
import { FormGroup } from "./constants";
import * as Types from "./types";

export const createUpdateDataAction = <T>(group: FormGroup) =>
    createAction(
        `SHL/UpdateData/${group}`,
        props<Types.PartialPayload<T>>()
    );

export const updateLoanInfoData = createUpdateDataAction<Types.LoanInfo>(FormGroup.LoanInfo);
export const updateLoanTermData = createUpdateDataAction<Types.LoanTerm>(FormGroup.LoanTerm);
export const updateAreThereCodebtorsData = createUpdateDataAction<Types.AreThereCodebtors>(FormGroup.IsCodebtors);
export const updateCodebtorInfoData = createUpdateDataAction<Types.CodebtorInfo>(FormGroup.CodebtorInfo);
export const updateIncomeInfoData = createUpdateDataAction<Types.IncomeInfo>(FormGroup.IncomeInfo);
export const updateLiabilityInfoData = createUpdateDataAction<Types.LiabilityInfo>(FormGroup.LiabilityInfo);

export const ActionsByGroupMap: Record<FormGroup, ActionCreator> = {
    [FormGroup.LoanInfo]: updateLoanInfoData,
    [FormGroup.LoanTerm]: updateLoanTermData,
    [FormGroup.IsCodebtors]: updateAreThereCodebtorsData,
    [FormGroup.CodebtorInfo]: updateCodebtorInfoData,
    [FormGroup.IncomeInfo]: updateIncomeInfoData,
    [FormGroup.LiabilityInfo]: updateLiabilityInfoData
};
