import { FormGroup } from "@angular/forms";

export const isFormGroupValid = (fg: FormGroup) => fg.valid;