import { AppState } from '../../state';
import { FormGroup, SHL_FEATURE_KEY } from "./constants";
import { LoanInfo, LoanTerm, AreThereCodebtors, CodebtorInfo, IncomeInfo, LiabilityInfo } from './types';

export const createFormGroupDataSelector = <T>(group: FormGroup) =>
    (state: AppState) => (state[SHL_FEATURE_KEY][group] as unknown) as T;

export const getStep1Data = createFormGroupDataSelector<LoanInfo>(FormGroup.LoanInfo);
export const getStep2Data = createFormGroupDataSelector<LoanTerm>(FormGroup.LoanTerm);
export const getStep3Data = createFormGroupDataSelector<AreThereCodebtors>(FormGroup.IsCodebtors);
export const getStep4Data = createFormGroupDataSelector<CodebtorInfo>(FormGroup.CodebtorInfo);
export const getStep5Data = createFormGroupDataSelector<IncomeInfo>(FormGroup.IncomeInfo);
export const getStep6Data = createFormGroupDataSelector<LiabilityInfo>(FormGroup.LiabilityInfo);

export const getCodebtorsExist = (state: AppState) => state[SHL_FEATURE_KEY][FormGroup.IsCodebtors]?.codebtors === 'yes';