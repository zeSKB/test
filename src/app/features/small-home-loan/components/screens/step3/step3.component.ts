import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep3Data } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { AreThereCodebtors } from '../../../types';
import { updateAreThereCodebtorsData } from '../../../actions';

@Component({
  selector: 'step3-screen',
  templateUrl: './step3.component.html',
  styleUrls: ['./step3.component.css']
})
export class Step3Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      codebtors: [null, [Validators.required]]
    },
    {
      updateOn: 'change'
    }
  );

  areThereCodebtorsControl = this.stepForm.get('codebtors');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);

    this._store
      .select(getStep3Data)
      .pipe(take(1))
      .subscribe(
        (data: AreThereCodebtors) => this.stepForm.patchValue(data || {}, { emitEvent: false })
      );

    this.areThereCodebtorsControl?.valueChanges.subscribe(
      (value: string) => {console.log(value) ;this._store.dispatch(
        updateAreThereCodebtorsData({ payload: { codebtors: value } })
      )}
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    if (this.areThereCodebtorsControl?.value === 'yes') {
      this._router.navigate([StepRoutes.Step4]);
    } else {
      this._router.navigate([StepRoutes.Step5]);
    }

  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Step2]);
  };

}
