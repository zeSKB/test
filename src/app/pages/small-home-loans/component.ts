import { Component, OnInit } from '@angular/core';
import { StepRoutes } from 'src/app/features/small-home-loan/constants';

@Component({
  selector: 'small-home-loans',
  templateUrl: './template.html',
  styleUrls: ['./styles.css']
})
export class SmallHomeLoansComponent implements OnInit {
  formRoute: string = '/' + StepRoutes.Introduction;

  constructor() { }

  ngOnInit(): void {
  }

}
