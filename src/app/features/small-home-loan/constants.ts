export const enum FormGroup {
    LoanInfo = 'loanInfo',
    LoanTerm = 'loanTerm',
    IsCodebtors = 'isCodebtors',
    CodebtorInfo = 'codebtorInfo',
    IncomeInfo = 'incomeInfo',
    LiabilityInfo = 'liabilityInfo'
}

export const enum StepRoutes {
    Introduction = 'small-home-loans/appication-form',
    Step1 = 'small-home-loans/appication-form/1',
    Step2 = 'small-home-loans/appication-form/2',
    Step3 = 'small-home-loans/appication-form/3',
    Step4 = 'small-home-loans/appication-form/4',
    Step5 = 'small-home-loans/appication-form/5',
    Step6 = 'small-home-loans/appication-form/6',
    Summary = 'small-home-loans/application-form/review',
    End = 'small-home-loans/application-sent'
}

export const SHL_FEATURE_KEY = 'shlForm';

export const FormFieldToLabelMap: Record<string, string> = {
    amount: 'Paskolos dydis',
    duration: 'Paskolos trukmė',
    codebtors: 'Ar yra bendraskolių?',
    codebtorInfo: 'Kas tie bendraskoliai? Vardas pavardė:',
    totalIncome: 'Kiek uždirbi per mėnesį?',
    totalLiabilities: 'Kiek įsipareigojęs per mėnesį?'
};