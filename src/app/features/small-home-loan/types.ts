export interface LoanInfo {
  amount: string;
}

export interface LoanTerm {
  duration: number;
}

export interface AreThereCodebtors {
  codebtors: string;
}

export interface CodebtorInfo {
    codebtorInfo: string;
}

export interface IncomeInfo {
    totalIncome: number;
}

export interface LiabilityInfo {
    totalLiabilities: number;
}

export type AnyFormGroup = LoanInfo | LoanTerm | AreThereCodebtors | CodebtorInfo | IncomeInfo | LiabilityInfo;

export type PartialPayload<T> = { payload: Partial<T> };
