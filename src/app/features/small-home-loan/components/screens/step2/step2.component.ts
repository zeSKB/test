import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep2Data } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { LoanTerm } from '../../../types';
import { updateLoanTermData } from '../../../actions';

@Component({
  selector: 'step2-screen',
  templateUrl: './step2.component.html',
  styleUrls: ['./step2.component.css']
})
export class Step2Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      duration: [null, [Validators.required]]
    },
    {
      updateOn: 'blur'
    }
  );

  loanDurationControl = this.stepForm.get('duration');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);

    this._store
      .select(getStep2Data)
      .pipe(take(1))
      .subscribe(
        (term: LoanTerm) => this.stepForm.patchValue(term || {}, { emitEvent: false })
      );

    this.loanDurationControl?.valueChanges.subscribe(
      (value: string) => this._store.dispatch(
        updateLoanTermData({ payload: { duration: Number(value) } })
      )
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    this._router.navigate([StepRoutes.Step3]);
  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Step1]);
  };

}
