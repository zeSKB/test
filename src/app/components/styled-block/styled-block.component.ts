import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'styled-block',
  templateUrl: './styled-block.component.html',
  styleUrls: ['./styled-block.component.css']
})
export class StyledBlockComponent implements OnInit {
  @Input() title: string = '';

  constructor() { }

  ngOnInit(): void {
  }

}
