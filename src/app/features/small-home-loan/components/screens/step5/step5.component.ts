import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep5Data, getCodebtorsExist } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { IncomeInfo } from '../../../types';
import { updateIncomeInfoData } from '../../../actions';

@Component({
  selector: 'step5',
  templateUrl: './step5.component.html',
  styleUrls: ['./step5.component.css']
})
export class Step5Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      totalIncome: [null, [Validators.required]]
    },
    {
      updateOn: 'blur'
    }
  );

  incomeInfoControl = this.stepForm.get('totalIncome');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);  

    this._store
      .select(getStep5Data)
      .pipe(take(1))
      .subscribe(
        (data: IncomeInfo) => this.stepForm.patchValue(data || {}, { emitEvent: false })
      );

    this.incomeInfoControl?.valueChanges.subscribe(
      (value: number) => this._store.dispatch(
        updateIncomeInfoData({ payload: { totalIncome: value } })
      )
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    this._router.navigate([StepRoutes.Step6]);
  };

  onClickedPrev() {
    this._store
      .select(getCodebtorsExist)
      .pipe(take(1))
      .subscribe(
        (isStep4Enabled: boolean) => {
            this._router.navigate([isStep4Enabled ? StepRoutes.Step4 :StepRoutes.Step3]);
        }
      );
    
  };
}
