import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes } from 'src/app/features/small-home-loan/constants';

@Component({
  selector: 'introduction-screen',
  templateUrl: './introduction.component.html',
  styleUrls: ['./introduction.component.css']
})
export class IntroductionComponent implements OnInit {

  constructor(private _router: Router) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
  }

  onClickedNext() {
    this._router.navigate([StepRoutes.Step1]);
  };

}
