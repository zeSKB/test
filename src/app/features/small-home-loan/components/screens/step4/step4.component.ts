import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep4Data } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { CodebtorInfo } from '../../../types';
import { updateCodebtorInfoData } from '../../../actions';

@Component({
  selector: 'step4',
  templateUrl: './step4.component.html',
  styleUrls: ['./step4.component.css']
})
export class Step4Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      codebtorInfo: [null, [Validators.required]]
    },
    {
      updateOn: 'blur'
    }
  );

  codebtorInfoControl = this.stepForm.get('codebtorInfo');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);
  
    this._store
      .select(getStep4Data)
      .pipe(take(1))
      .subscribe(
        (data: CodebtorInfo) => this.stepForm.patchValue(data || {}, { emitEvent: false })
      );

    this.codebtorInfoControl?.valueChanges.subscribe(
      (value: string) => this._store.dispatch(
        updateCodebtorInfoData({ payload: { codebtorInfo: value } })
      )
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    this._router.navigate([StepRoutes.Step5]);
  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Step3]);
  };
}
