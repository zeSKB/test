import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { StepRoutes, FormFieldToLabelMap } from 'src/app/features/small-home-loan/constants';
import { Validators, FormBuilder } from '@angular/forms';
import { Store } from '@ngrx/store';
import { AppState } from 'src/app/state';
import { isFormGroupValid } from "src/app/features/small-home-loan/functions";
import { getStep6Data } from "src/app/features/small-home-loan/selectors";
import { take } from 'rxjs/operators';
import { LiabilityInfo } from '../../../types';
import { updateLiabilityInfoData } from '../../../actions';

@Component({
  selector: 'step6',
  templateUrl: './step6.component.html',
  styleUrls: ['./step6.component.css']
})
export class Step6Component implements OnInit {
  fieldToLabelMap = FormFieldToLabelMap;
  stepForm = this._fb.group(
    {
      totalLiabilities: [null, [Validators.required]]
    },
    {
      updateOn: 'blur'
    }
  );

  totalLiabilitiesControl = this.stepForm.get('totalLiabilities');
  showValidationMessages = false;

  constructor(
    private _router: Router,
    private _fb: FormBuilder,
    private _store: Store<AppState>
  ) { }

  ngOnInit(): void {
    this.onClickedNext = this.onClickedNext.bind(this);
    this.onClickedPrev = this.onClickedPrev.bind(this);

    this._store
      .select(getStep6Data)
      .pipe(take(1))
      .subscribe(
        (data: LiabilityInfo) => this.stepForm.patchValue(data, { emitEvent: false })
      );

    this.totalLiabilitiesControl?.valueChanges.subscribe(
      (value: number) => this._store.dispatch(
        updateLiabilityInfoData({ payload: { totalLiabilities: value } })
      )
    );
  }

  onClickedNext() {
    if (!isFormGroupValid(this.stepForm)) {
      this.showValidationMessages = true;

      return;
    }

    this._router.navigate([StepRoutes.Summary]);
  };

  onClickedPrev() {
    this._router.navigate([StepRoutes.Step5]);
  };

}
