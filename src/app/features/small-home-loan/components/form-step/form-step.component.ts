import { Component, OnInit, Input } from '@angular/core';
import { AnyFn } from '@ngrx/store/src/selector';

@Component({
  selector: 'form-step',
  templateUrl: './form-step.component.html',
  styleUrls: ['./form-step.component.css']
})



export class FormStepComponent implements OnInit {
  @Input() title: string = '';
  @Input() onClickedNext: Optional<AnyFn> = undefined;
  @Input() onClickedPrev: Optional<AnyFn> = undefined;

  constructor() { }

  ngOnInit(): void {
  }

  handleClickedPrev () {
    if (this.onClickedPrev) {
      this.onClickedPrev();
    }
  }

  handleClickedNext () {
    if (this.onClickedNext) {
      this.onClickedNext();
    }
  }

}
