import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SmallHomeLoansComponent } from './component';

describe('SmallHomeLoansComponent', () => {
  let component: SmallHomeLoansComponent;
  let fixture: ComponentFixture<SmallHomeLoansComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SmallHomeLoansComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SmallHomeLoansComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
