import { FormGroup } from "./constants";
import {
    LoanInfo,
    LoanTerm,
    AreThereCodebtors,
    CodebtorInfo,
    IncomeInfo,
    LiabilityInfo
} from "./types";

export interface SmallHomeLoanState {
    [FormGroup.LoanInfo]: Optional<LoanInfo>,
    [FormGroup.LoanTerm]: Optional<LoanTerm>,
    [FormGroup.IsCodebtors]: Optional<AreThereCodebtors>,
    [FormGroup.CodebtorInfo]: Optional<CodebtorInfo>;
    [FormGroup.IncomeInfo]: Optional<IncomeInfo>;
    [FormGroup.LiabilityInfo]: Optional<LiabilityInfo>;
};
