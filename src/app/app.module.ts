import { BrowserModule, Title } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { StoreModule } from '@ngrx/store';
import { SiteHeaderComponent } from './components/site-header/site-header.component';
import { SiteLogoComponent } from './components/site-logo/site-logo.component';
import { StyledBlockComponent } from './components/styled-block/styled-block.component';
import { IndexComponent } from './pages/index/index.component';
import { SmallHomeLoansComponent } from './pages/small-home-loans/component';
import { reducers, metaReducers } from "./state/index";
import { SmallHomeLoanModule } from "./features/small-home-loan/small-home-loan.module";

@NgModule({
  declarations: [
    AppComponent,
    SiteHeaderComponent,
    SiteLogoComponent,
    IndexComponent,
    StyledBlockComponent,
    SmallHomeLoansComponent
  ],
  imports: [
    SmallHomeLoanModule,
    BrowserModule,
    AppRoutingModule,
    StoreModule.forRoot(reducers, { metaReducers })
  ],
  providers: [Title],
  bootstrap: [AppComponent]
})

export class AppModule { }
