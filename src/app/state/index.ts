import { ActionReducerMap, MetaReducer } from '@ngrx/store';
import { SmallHomeLoanState } from "../features/small-home-loan/state";
import { shlFormReducer } from "../features/small-home-loan/reducers";
import { SHL_FEATURE_KEY } from "../features/small-home-loan/constants";

export interface AppState {
  [SHL_FEATURE_KEY]: SmallHomeLoanState;
}

export const reducers: ActionReducerMap<AppState> = {
  [SHL_FEATURE_KEY]: shlFormReducer
};

export const metaReducers: MetaReducer<AppState>[] = [];
