import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { IndexComponent as IndexPageComponent } from './pages/index/index.component';
import { IntroductionComponent } from './features/small-home-loan/components/screens/introduction/introduction.component';
import { Step1Component } from './features/small-home-loan/components/screens/step1/step1.component';
import { Step2Component } from './features/small-home-loan/components/screens/step2/step2.component';
import { Step3Component } from './features/small-home-loan/components/screens/step3/step3.component';
import { Step4Component } from './features/small-home-loan/components/screens/step4/step4.component';
import { Step5Component } from './features/small-home-loan/components/screens/step5/step5.component';
import { Step6Component } from './features/small-home-loan/components/screens/step6/step6.component';
import { SummaryComponent } from './features/small-home-loan/components/screens/summary/summary.component';
import { EndScreenComponent } from './features/small-home-loan/components/screens/end-screen/end-screen.component';
import { SmallHomeLoansComponent } from './pages/small-home-loans/component';
import { StepRoutes } from 'src/app/features/small-home-loan/constants';

const routes: Routes = [
    {
        path: StepRoutes.Introduction,
        component: IntroductionComponent,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step1,
        component: Step1Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step2,
        component: Step2Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step3,
        component: Step3Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step4,
        component: Step4Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step5,
        component: Step5Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Step6,
        component: Step6Component,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.Summary,
        component: SummaryComponent,
        data: { title: 'Prašymas vartojimo paskolai suteikti' }
    },
    {
        path: StepRoutes.End,
        component: EndScreenComponent,
        data: { title: 'Prašymas vartojimo paskolai suteikti pateiktas' }
    },
    {
       path: 'small-home-loans',
       component: SmallHomeLoansComponent,
       data: { title: 'Vartojimo paskola namams' }
    },
    {
        path: '',
        component: IndexPageComponent
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
