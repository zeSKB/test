import { SmallHomeLoanState as State, SmallHomeLoanState } from "./state";
import { createReducer, combineReducers, on, ActionReducer } from '@ngrx/store';
import { ActionsByGroupMap } from "./actions";
import { FormGroup } from "./constants";

const defaultEmpty = undefined;

const defaultState: State = {
    [FormGroup.LoanInfo]: defaultEmpty,
    [FormGroup.LoanTerm]: defaultEmpty,
    [FormGroup.IsCodebtors]: defaultEmpty,
    [FormGroup.CodebtorInfo]: defaultEmpty,
    [FormGroup.IncomeInfo]: defaultEmpty,
    [FormGroup.LiabilityInfo]: defaultEmpty,
};

const createReducerCreator = (group: FormGroup) =>
    createReducer(
        defaultState[group],
        on(
            ActionsByGroupMap[group],
            (state: any, { payload }: any) => ({
                ...state,
                ...payload
            })
        )
    ) as any;


const loanInfoReducer = createReducerCreator(FormGroup.LoanInfo);
const loanTermReducer = createReducerCreator(FormGroup.LoanTerm);
const isCodebtorsReducer = createReducerCreator(FormGroup.IsCodebtors);
const codebtorInfoReducer = createReducerCreator(FormGroup.CodebtorInfo);
const incomeInfoReducer = createReducerCreator(FormGroup.IncomeInfo);
const liabilityInfoReducer = createReducerCreator(FormGroup.LiabilityInfo);

export const shlFormReducer = combineReducers({
    [FormGroup.LoanInfo]: loanInfoReducer,
    [FormGroup.LoanTerm]: loanTermReducer,
    [FormGroup.IsCodebtors]: isCodebtorsReducer,
    [FormGroup.CodebtorInfo]: codebtorInfoReducer,
    [FormGroup.IncomeInfo]: incomeInfoReducer,
    [FormGroup.LiabilityInfo]: liabilityInfoReducer
}, defaultState) as ActionReducer<SmallHomeLoanState>;

