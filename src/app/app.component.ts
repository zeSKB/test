import { Component } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Swedbank';

  constructor (
    private _activatedRoute: ActivatedRoute,
    private _router: Router,
    private _titleService: Title
  ) {}

  ngOnInit () {
    this._router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.setTitleByActivatedRoute(this._activatedRoute);
      }
    });
  }

  private setTitleByActivatedRoute (route: ActivatedRoute) {
    let pageTitle = '';

    if (route.firstChild) {
      const { title } = route.firstChild.snapshot.data;

      pageTitle = title;
    }

    this._titleService.setTitle(pageTitle);
    this.title = pageTitle;
  }
}
