import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StyledBlockComponent } from './styled-block.component';

describe('StyledBlockComponent', () => {
  let component: StyledBlockComponent;
  let fixture: ComponentFixture<StyledBlockComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StyledBlockComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StyledBlockComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
